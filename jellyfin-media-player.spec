Name:			jellyfin-media-player
Version:		1.11.1
Release:		%autorelease
Summary:		Jellyfin Desktop Client based on Plex Media Player
License:		GPLv2
URL:			https://jellyfin.org
Source0:		https://github.com/jellyfin/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	libtool
BuildRequires:	freetype-devel
BuildRequires:	libXrandr-devel
BuildRequires:	libvdpau-devel
BuildRequires:	libva-devel
BuildRequires:	mesa-libGL-devel
BuildRequires:	libdrm-devel
BuildRequires:	libX11-devel
BuildRequires:	mesa-libEGL-devel
BuildRequires:	yasm
BuildRequires:	alsa-lib
BuildRequires:	pulseaudio-libs-devel
BuildRequires:	zlib-devel
BuildRequires:	fribidi-devel
BuildRequires:	git
BuildRequires:	gnutls-devel
BuildRequires:	mesa-libGLU-devel
BuildRequires:	SDL2-devel
BuildRequires:	cmake
BuildRequires:	wget
BuildRequires:	gcc-c++
BuildRequires:	qt-devel
BuildRequires:	libcec-devel
BuildRequires:	qt5-qtbase-devel
BuildRequires:	curl
BuildRequires:	unzip
BuildRequires:	qt5-qtwebchannel-devel
BuildRequires:	qt5-qtwebengine-devel
BuildRequires:	qt5-qtx11extras-devel
BuildRequires:	qwt-qt5-devel
BuildRequires:	qt5-qtbase-private-devel
BuildRequires:	mpv-devel

Requires:		qt5-qtquickcontrols

%description
Desktop client using jellyfin-web with embedded MPV player. Supports Windows, Mac OS, and Linux. Media plays within the same window using the jellyfin-web interface unlike Jellyfin Desktop. Supports audio passthrough. Based on Plex Media Player.

%prep
%autosetup -n %{name}-%{version}

%build
%cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo
%cmake_build

%install
%cmake_install

%files
%license LICENSE
%{_bindir}/jellyfinmediaplayer
%{_datadir}/jellyfinmediaplayer/web-client/extension/*
%{_datadir}/applications/com.github.iwalton3.jellyfin-media-player.desktop
%{_datadir}/metainfo/com.github.iwalton3.jellyfin-media-player.appdata.xml
%{_datadir}/icons/hicolor/scalable/apps/com.github.iwalton3.jellyfin-media-player.svg

%changelog
%autochangelog
